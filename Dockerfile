FROM php:5.6.25-zts

ENV EBOT_HOME="/opt/ebot" \
    TIMEZONE="Europe/Paris"

RUN curl -sL https://deb.nodesource.com/setup_6.x | bash -
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update -y
RUN apt-get install -y git nodejs yarn build-essential php5-curl netcat
RUN pecl install pthreads-2.0.10
RUN docker-php-ext-enable pthreads

RUN mkdir -p ${EBOT_HOME}
RUN cd ${EBOT_HOME}
RUN yarn add socket.io@0.9.12 archiver formidable
RUN yarn global add forever
RUN docker-php-ext-install mysql sockets

RUN echo 'date.timezone = "${TIMEZONE}"' >> /usr/local/etc/php/conf.d/php.ini

RUN git clone https://github.com/deStrO/eBot-CSGO.git "$EBOT_HOME"
WORKDIR ${EBOT_HOME}
ENV TAG "master"

RUN curl -sS https://getcomposer.org/installer | php
RUN git checkout $TAG
RUN php composer.phar install
RUN cp "$EBOT_HOME"/config/config.ini.smp "$EBOT_HOME"/config/config.ini


COPY entrypoint.sh /sbin/entrypoint.sh

RUN chmod +x /sbin/entrypoint.sh

EXPOSE 12360 12360/udp 12361/udp

ENTRYPOINT ["/sbin/entrypoint.sh"]
